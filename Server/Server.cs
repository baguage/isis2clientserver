﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetworkCommsDotNet;
using Isis;

namespace ServerApplication
{
    class Server
    {
        Group isisGroup;
        String str;
        delegate void myLhandler(string who);
        delegate void stringArg(string who);
        delegate void intArgs(int id, int val); 

        static int LOOKUP = 0;
        static int UPDATE = 1;

        public Server()
        {
            // TODO: str string should be queried on startup, because group already has some state

            String str = "";


            isisGroup = new Group("IsisReplicatedServer");

            isisGroup.Handlers[LOOKUP] += (myLhandler)delegate(string who)
            {
                Console.WriteLine("My LOOKUP handler was asked to look up {0}", who);
                // Return current state
                isisGroup.Reply(str);
            };

            isisGroup.Handlers[UPDATE] += (stringArg)delegate(string name)
            {
                Console.WriteLine("Update {0}", name);
                str = name;
            };

            isisGroup.Join();

        }

        public void Start()
        {
            //Trigger the method PrintIncomingMessage when a packet of type 'Message' is received
            //We expect the incoming object to be a string which we state explicitly by using <string>
            NetworkComms.AppendGlobalIncomingPacketHandler<string>("Message", PrintIncomingMessage);
            //Start listening for incoming connections
            TCPConnection.StartListening(true);

            //Print out the IPs and ports we are now listening on
            Console.WriteLine("Server listening for TCP connection on:");
            foreach (System.Net.IPEndPoint localEndPoint in TCPConnection.ExistingLocalListenEndPoints()) Console.WriteLine("{0}:{1}", localEndPoint.Address, localEndPoint.Port);

        }

        public void Stop()
        {
            //We have used NetworkComms so we should ensure that we correctly call shutdown
            NetworkComms.Shutdown();
        }

        private void PrintIncomingMessage(PacketHeader header, Connection connection, string message)
        {
            Console.WriteLine("\nA message was recieved from " + connection.ToString() + " which said '" + message + "'.");
            isisGroup.Send(UPDATE, message);
  
        }

    }

    class Program
    {


        static void Main(string[] args)
        {
            IsisSystem.Start();
            Server server = new Server();
            server.Start();

            //Let the user close the server
            Console.WriteLine("\nPress any key to close server.");
            Console.ReadKey(true);
            
            server.Stop();
            IsisSystem.Shutdown();
        }
    }
}